**Weight converter**

Gem for converting weight values between different units and colorizing the output. Run 'weight_output' with 4 parameters (number, given unit, desired unit, color of the text).

For example, if you want to convert 25 g to kg and see green text type `weight_converter 25 g kg green`

**Supported units**:

- kg - kilogram
- t - ton
- cent - centner
- ct - carat
- g - gram
- mg - miligram
- mkg - microgram
- lot - long ton
- sht - short ton
- kip - kip
- st - stone
- lb - pound
- oz - ounce
- dr - drachma
- gr - gran
- lbt - troy pound
- ozt - troy ounce
- dwt - pennyweight
