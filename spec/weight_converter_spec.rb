require "weight_converter"

describe WeightConverter do
    weight_converter = WeightConverter.new
    describe "#convert_to_g" do
        context "convert weight in given units to grams" do
            it "return 100_000" do
                expect(weight_converter.convert_to_g(100, :kg)). to be_within(0.1).of(100_000)
            end
    
            it "return 22_679_600" do
                expect(weight_converter.convert_to_g(50, :kip)). to be_within(0.1).of(22_679_600)
            end
    
            it "return 311" do
                expect(weight_converter.convert_to_g(10, :ozt)). to be_within(0.1).of(311)
            end
        end
    end

    describe "#convert_g_to" do
        context "convert weight in grams to given units" do
            it "return 500" do
                expect(weight_converter.convert_g_to(1000, :ct)). to be_within(0.1).of(5000)
            end
            
            it "return 588" do
                expect(weight_converter.convert_g_to(2000, :dr)). to be_within(0.1).of(588.235294118)
            end
    
            it "return 0.0001" do
                expect(weight_converter.convert_g_to(10, :cent)). to be_within(0.1).of(0.0001)
            end
        end
    end
end