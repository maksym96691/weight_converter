require 'colorize'

class WeightConverter
    @@weights = {
        "kg": 1000,
        "t": 1_000_000,
        "cent": 100_000,
        "ct": 0.2,
        "g": 1,
        "mg": 0.001,
        "mkg": 0.000001,
        "lot": 1_016_000,
        "sht": 907_180,
        "kip": 453592,
        "st": 6350.29,
        "lb": 453.592,
        "oz": 28.3495,
        "dr": 3.4,
        "gr": 0.065,
        "lbt": 373.24,
        "ozt": 31.1,
        "dwt": 1.55517,
    }

    def convert(number, from, to, color)
        return colorize(convert_g_to(convert_to_g(number.to_f, from), to).to_s, color)
    end

    def convert_to_g(number, from)
        return number * @@weights[from]  
    end

    def convert_g_to(number, to)
        return number / @@weights[to] 
    end

    def colorize(string, color)
        string.colorize(color)
    end
end
