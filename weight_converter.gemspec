Gem::Specification.new do |s|

    s.name        = 'weight_converter'
    s.version     = '1.0.2'
    s.date        = '2020-04-17'
    s.summary     = "weight_converter"
    s.executables << 'weight_converter'
    s.description = "Gem for converting weight value between different units and colorizing the output"
    s.authors     = ["Mereum"]
    s.email       = 'maksym.sychov14@gmail.com'
    s.files       = ["lib/weight_converter.rb",
      "weight_converter.gemspec",
      "spec/weight_converter_spec.rb",
      "Rakefile",
      "Gemfile",
      "README.md",
      "bin/weight_converter"
    ]
    s.test_files = [
      'spec/weight_converter_spec.rb'
    ]
    s.homepage    =
      'https://gitlab.com/maksym96691/weight_converter'
    s.license       = 'MIT'
end
